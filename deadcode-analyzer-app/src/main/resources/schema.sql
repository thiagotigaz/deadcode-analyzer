CREATE TABLE IF NOT EXISTS T_REPOSITORY (
  id  IDENTITY PRIMARY KEY NOT NULL,
  url VARCHAR(255)
);

CREATE TABLE IF NOT EXISTS T_JOB (
  id            IDENTITY PRIMARY KEY NOT NULL,
  id_repository BIGINT               NOT NULL,
  branch        VARCHAR(255),
  revision      VARCHAR(255),
  status        VARCHAR(255)         NOT NULL,
  FOREIGN KEY (id_repository)
  REFERENCES T_REPOSITORY (id)
);