package com.devfactory.analyzer.service;

import com.devfactory.analyzer.model.Job;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 * This service is responsible to run the code analyzes job asynchronously. It uses the Executor defined in {@link
 * com.devfactory.analyzer.Application} class.
 */
@Service
public class JobService {

    @Async
    public void startJob(Job job) {
        try {
            System.out.println("Starting job " + job.getRepository().getUrl());
            Thread.sleep(10000);
            System.out.println("Finished job");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
