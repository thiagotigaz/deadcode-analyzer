package com.devfactory.analyzer.service;

import com.devfactory.analyzer.dao.JobDAO;
import com.devfactory.analyzer.dao.RepositoryDAO;
import com.devfactory.analyzer.dto.AnalyzerReqDTO;
import com.devfactory.analyzer.model.Job;
import com.devfactory.analyzer.model.JobStatus;
import com.devfactory.analyzer.model.Repository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * The goal of this service is to deal with the transactions needed to start a new Job
 * and also pass the Job to the service responsible to do the analyzes.(Run the task)
 */
@Service
public class AnalyzerService {

    private JobService jobService;
    private RepositoryDAO repositoryDAO;
    private JobDAO jobDAO;

    @Autowired
    public AnalyzerService(JobService jobService, RepositoryDAO repositoryDAO, JobDAO jobDAO) {
        this.jobService = jobService;
        this.repositoryDAO = repositoryDAO;
        this.jobDAO = jobDAO;
    }

    /**
     * This method is responsible to create a new repository entity and start a new analyses using
     * the latest revision of master branch
     *
     * @param dto object which should contain at least a valid repository url
     */
    @Transactional
    public Job insertRepoAndStartJob(AnalyzerReqDTO dto) {
        Repository repository = Repository.builder().url(dto.getUrl()).build();
        repository = repositoryDAO.create(repository);

        // Add a new job for the given repo
        Job job = Job.builder()
            .repository(repository)
            .branch(dto.getBranch() != null ? dto.getBranch() : "master")
            .revision(dto.getRevision())
            .status(JobStatus.PROCESSING)
            .build();
        // Persists the job
        job = jobDAO.create(job);
        jobService.startJob(job);

        return job;
    }
}
