package com.devfactory.analyzer.dao;

import com.devfactory.analyzer.model.Repository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;

/**
 *
 */
@org.springframework.stereotype.Repository
public class RepositoryDAO {

    public static final String TABLE_NAME = "T_REPOSITORY";
    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;
    private final SimpleJdbcInsert jdbcInsert;

    @Autowired
    public RepositoryDAO(JdbcTemplate jdbcTemplate) {
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);
        this.jdbcInsert = new SimpleJdbcInsert(jdbcTemplate)
            .withTableName(TABLE_NAME)
            .usingGeneratedKeyColumns("id")
            .usingColumns("url");
    }

    public Repository create(Repository repository) {
        MapSqlParameterSource map = new MapSqlParameterSource()
            .addValue("url", repository.getUrl());
        Number idNumber = jdbcInsert.executeAndReturnKey(map);
        repository.setId(idNumber.longValue());

        return repository;
    }

    public Repository findById(long id) {
        MapSqlParameterSource map = new MapSqlParameterSource()
            .addValue("id", id);

        return namedParameterJdbcTemplate
            .queryForObject("SELECT * FROM " + TABLE_NAME + " WHERE id = :id", map
                , new BeanPropertyRowMapper<>(Repository.class));
    }
}
