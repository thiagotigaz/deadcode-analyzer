package com.devfactory.analyzer.dao;

import com.devfactory.analyzer.model.Job;
import com.devfactory.analyzer.model.JobStatus;
import com.devfactory.analyzer.model.Repository;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;

@org.springframework.stereotype.Repository
public class JobDAO {

    public static final String TABLE_NAME = "T_JOB";
    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;
    private final SimpleJdbcInsert jdbcInsert;

    @Autowired
    public JobDAO(JdbcTemplate jdbcTemplate) {
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);
        this.jdbcInsert = new SimpleJdbcInsert(jdbcTemplate)
            .withTableName(TABLE_NAME)
            .usingGeneratedKeyColumns("id")
            .usingColumns("id_repository", "branch", "revision", "status");
    }

    public Job create(Job job) {
        MapSqlParameterSource map = new MapSqlParameterSource()
            .addValue("id_repository", job.getRepository().getId())
            .addValue("branch", job.getBranch())
            .addValue("revision", job.getRevision())
            .addValue("status", job.getStatus().name());
        Number idNumber = jdbcInsert.executeAndReturnKey(map);
        job.setId(idNumber.longValue());

        return job;
    }


    public List<Job> findAll() {
        String query = String.format("SELECT * FROM %s j, %s r WHERE j.id_repository = r.id", TABLE_NAME,
            RepositoryDAO.TABLE_NAME);
        return namedParameterJdbcTemplate.query(query, new RowMapper<Job>() {
            @Override
            public Job mapRow(ResultSet rs, int rowNum) throws SQLException {
                ResultSetMetaData rsmd = rs.getMetaData();
                Repository gitRepo = Repository.builder()
                    .id(rs.getLong(6))
                    .url(rs.getString(7))
                    .build();
                Job job = Job.builder()
                    .repository(gitRepo)
                    .id(rs.getLong(1))
                    .branch(rs.getString(3))
                    .revision(rs.getString(4))
                    .status(JobStatus.valueOf(rs.getString(5)))
                    .build();
                return job;
            }
        });
    }
}
