package com.devfactory.analyzer.controller;

import com.devfactory.analyzer.dao.JobDAO;
import com.devfactory.analyzer.dto.AnalyzerReqDTO;
import com.devfactory.analyzer.model.Job;
import com.devfactory.analyzer.service.AnalyzerService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * This controller is responsible to interact with the GithubRepoService to
 * add and list repositories.
 */
@RestController
@RequestMapping(path = "/repo", produces = MediaType.APPLICATION_JSON_VALUE)
public class AnalyzerController {

    @Autowired
    private AnalyzerService analyzerService;

    @Autowired
    private JobDAO jobDAO;

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity addRepo(@RequestBody @Valid AnalyzerReqDTO analyzerReqDTO) {
        analyzerService.insertRepoAndStartJob(analyzerReqDTO);
        return new ResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<Job> listJobs() {
        // TODO return a proper DTO just with necessary information
        return jobDAO.findAll();
    }
}
