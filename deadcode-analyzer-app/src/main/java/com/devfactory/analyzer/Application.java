package com.devfactory.analyzer;

import java.util.concurrent.Executor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * Entry point of the application.
 */
@SpringBootApplication
@EnableAsync
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    /**
     * For the simplicity and time of this assignment we are going to use async tasks instead of having a more
     * complex architecture using a queue and a worker to consume it.
     */
    @Bean
    public Executor asyncTaskExecutor() {
        return new SimpleAsyncTaskExecutor();
    }
}
