package com.devfactory.analyzer.dto;

import java.io.Serializable;
import lombok.Builder;
import lombok.Getter;
import lombok.Value;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * Data Transfer Object used to pass repository and info needed by Job at once
 */
@Value
@Getter
@Builder
public class AnalyzerReqDTO implements Serializable {

    @NotEmpty
    private final String url;
    private final String branch;
    private final String revision;

}
