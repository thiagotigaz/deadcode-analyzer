package com.devfactory.analyzer;

import com.devfactory.analyzer.dto.AnalyzerReqDTO;
import com.devfactory.analyzer.model.Job;
import com.devfactory.analyzer.model.JobStatus;
import com.devfactory.analyzer.model.Repository;

public class TestsUtil {

    public static Repository createDummyRepo() {
        return Repository.builder()
            .url("http://dummyrepo.com/repo/123.git")
            .build();
    }

    public static Job createDummyJob(Repository repo) {
        return Job.builder()
            .repository(repo != null ? repo : createDummyRepo())
            .branch("master")
            .revision("latest")
            .status(JobStatus.PROCESSING)
            .build();
    }

    public static AnalyzerReqDTO createDummyAnalyzerReqDTO() {
        return AnalyzerReqDTO.builder()
            .url("http://dummyrepo.com/repo/123.git")
            .branch("master")
            .revision("latest")
            .build();
    }
}
