package com.devfactory.analyzer;

import com.devfactory.analyzer.config.AnalyzerProperties;
import javax.sql.DataSource;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.jdbc.core.JdbcTemplate;

@Configuration
@Import({DataSourceAutoConfiguration.class, AnalyzerProperties.class})
@ComponentScan("com.devfactory.analyzer.dao")
public class TestsConfig {

    @Bean
    public JdbcTemplate jdbcTemplate(DataSource ds) {
        return new JdbcTemplate(ds);
    }
}
