package com.devfactory.analyzer.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import com.devfactory.analyzer.dao.JobDAO;
import com.devfactory.analyzer.dao.RepositoryDAO;
import com.devfactory.analyzer.dto.AnalyzerReqDTO;
import com.devfactory.analyzer.model.Job;
import com.devfactory.analyzer.model.JobStatus;
import com.devfactory.analyzer.model.Repository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class AnalyzerServiceTest {

    @InjectMocks
    private AnalyzerService analyzerService;

    @Mock
    private JobService jobService;

    @Mock
    private RepositoryDAO repositoryDAO;

    @Mock
    private JobDAO jobDAO;

    @Test
    public void insertRepoAndStartJobTest() {
        String url = "http://dummyrepo.com/repo/123.git";
        Repository expectedRepo = Repository.builder().url(url).build();
        Repository returnedRepo = Repository.builder().id(1L).url(url).build();
        Job expectedJob = Job.builder().repository(returnedRepo).branch("master").status(JobStatus.PROCESSING).build();
        Job returnedJob = Job.builder().id(1L).repository(returnedRepo).branch("master").status(JobStatus.PROCESSING)
            .build();

        when(repositoryDAO.create(expectedRepo)).thenReturn(returnedRepo);
        when(jobDAO.create(expectedJob)).thenReturn(returnedJob);

        AnalyzerReqDTO dto = AnalyzerReqDTO.builder().url(url).build();
        Job job = analyzerService.insertRepoAndStartJob(dto);

        Mockito.verify(repositoryDAO).create(Matchers.eq(expectedRepo));
        Mockito.verify(jobDAO).create(Matchers.eq(expectedJob));
        Mockito.verify(jobService).startJob(Matchers.eq(job));
        assertEquals(returnedJob, job);
    }
}
