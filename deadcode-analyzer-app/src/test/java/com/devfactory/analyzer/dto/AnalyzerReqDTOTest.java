package com.devfactory.analyzer.dto;

import static org.junit.Assert.assertEquals;

import com.devfactory.analyzer.TestsUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import org.junit.Test;

public class AnalyzerReqDTOTest {

    @Test
    public void serializationTest() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        AnalyzerReqDTO expected = TestsUtil.createDummyAnalyzerReqDTO();
        String json = objectMapper.writeValueAsString(expected);
        AnalyzerReqDTO fromJson = objectMapper.readValue(json, AnalyzerReqDTO.class);
        assertEquals(expected, fromJson);
    }
}