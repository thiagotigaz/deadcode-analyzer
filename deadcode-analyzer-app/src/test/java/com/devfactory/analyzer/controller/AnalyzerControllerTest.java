package com.devfactory.analyzer.controller;

import com.devfactory.analyzer.dto.AnalyzerReqDTO;
import com.devfactory.analyzer.service.AnalyzerService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@RunWith(MockitoJUnitRunner.class)
public class AnalyzerControllerTest {

    private static final ObjectMapper mapper = new ObjectMapper();

    private MockMvc mockMvc;

    @InjectMocks
    private AnalyzerController analyzerController;

    @Mock
    private AnalyzerService analyzerService;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(analyzerController).build();
    }

    @Test
    public void testCreateRepoAndStartJob() throws Exception {
        String expectedRepoUrl = "http://asd.com";
        String expectedBranch = "master";
        String expectedRevision = "revision";
        AnalyzerReqDTO dto = AnalyzerReqDTO.builder()
            .url(expectedRepoUrl)
            .branch(expectedBranch)
            .revision(expectedRevision)
            .build();
        String json = mapper.writeValueAsString(dto);
        mockMvc.perform(MockMvcRequestBuilders.post("/repo")
            .content(json)
            .contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(MockMvcResultMatchers.status().isOk());

        Mockito.verify(analyzerService).insertRepoAndStartJob(Matchers.eq(dto));
    }
}
