package com.devfactory.analyzer.dao;

import static org.junit.Assert.assertEquals;

import com.devfactory.analyzer.TestsConfig;
import com.devfactory.analyzer.TestsUtil;
import com.devfactory.analyzer.model.Job;
import com.devfactory.analyzer.model.JobStatus;
import com.devfactory.analyzer.model.Repository;
import java.util.List;
import java.util.Map;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.ConfigFileApplicationContextInitializer;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestsConfig.class, JobDAOTests.class}, initializers =
    ConfigFileApplicationContextInitializer.class)
public class JobDAOTests {

    @Autowired
    private RepositoryDAO gitRepoDAO;

    @Autowired
    private JobDAO jobDAO;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Test
    public void createTest() {
        // We need to ensure repo is created first, otherwise job creation will fail because of the constraint
        Repository repo = createRepo();
        Job job = createJob(repo, "master");
        Map<String, Object> stringObjectMap = jdbcTemplate.queryForMap("SELECT * FROM t_job WHERE id = ?", job.getId());
        assertEquals(job.getId(), stringObjectMap.get("id"));
        assertEquals(job.getBranch(), stringObjectMap.get("branch"));
        assertEquals(job.getRevision(), stringObjectMap.get("revision"));
        assertEquals(job.getRepository().getId(), stringObjectMap.get("id_repository"));
        assertEquals(job.getStatus(), JobStatus.valueOf((String) stringObjectMap.get("status")));
    }

    @Test
    public void findAllTest() {
        // We need to ensure repo is created first, otherwise job creation will fail because of the constraint
        Repository repo = createRepo();
        createJob(repo, "master");
        createJob(repo, "dev");
        createJob(repo, "integration");
        createJob(repo, "feature/x");
        List<Job> all = jobDAO.findAll();
        assertEquals(4, all.size());
    }

    private Repository createRepo() {
        return gitRepoDAO.create(TestsUtil.createDummyRepo());
    }

    private Job createJob(Repository repo, String branch) {
        Job job = TestsUtil.createDummyJob(repo);
        job.setBranch(branch);
        return jobDAO.create(job);
    }
}
