package com.devfactory.analyzer.dao;

import static org.junit.Assert.assertEquals;

import com.devfactory.analyzer.TestsConfig;
import com.devfactory.analyzer.TestsUtil;
import com.devfactory.analyzer.model.Repository;
import java.util.Map;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.ConfigFileApplicationContextInitializer;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestsConfig.class, RepositoryDAOTest.class}, initializers =
    ConfigFileApplicationContextInitializer.class)
public class RepositoryDAOTest {

    @Autowired
    private RepositoryDAO repositoryDAO;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Test
    public void createTest() {
        Repository dummyRepo = TestsUtil.createDummyRepo();
        repositoryDAO.create(dummyRepo);
        Map<String, Object> stringObjectMap = jdbcTemplate.queryForMap(String.format("SELECT * FROM %s "
            + "WHERE id = ?", RepositoryDAO.TABLE_NAME), 1L);

        assertEquals(dummyRepo.getUrl(), stringObjectMap.get("url"));
    }

    @Test
    public void findById() {
        Repository dummyRepo = TestsUtil.createDummyRepo();
        Repository repository = repositoryDAO.create(dummyRepo);
        Repository retrievedRepo = repositoryDAO.findById(repository.getId());
        assertEquals(repository.getId(), retrievedRepo.getId());
        assertEquals(repository.getUrl(), retrievedRepo.getUrl());
    }
}