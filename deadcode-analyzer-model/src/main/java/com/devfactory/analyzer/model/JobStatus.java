package com.devfactory.analyzer.model;

public enum JobStatus {
    PROCESSING,
    COMPLETED,
    FAILED
}
