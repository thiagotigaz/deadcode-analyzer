package com.devfactory.analyzer.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * This POJO is responsible to keep the information of a repository analyzes.
 * The results of a job is related to a given version of the repository
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Job {

    // Auto generated id
    private Long id;

    private Repository repository;

    private String branch;

    private String revision;

    private JobStatus status;

}
