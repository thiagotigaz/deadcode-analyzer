package com.devfactory.analyzer.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * This POJO is responsible to keep github repository information
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Repository {

    // Auto generated id
    private Long id;

    // Github repo clone url
    private String url;

}
